const chai = require("chai");
const { assert } = require("chai");

//import and use chai-http to allow chai to send request to our server
const http = require("chai-http");
chai.use(http);
/*
describe("API Test Suite for users", () => {
  it("Test API get users is running", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        assert.isDefined(res);
        done();
      });
  });
  it("Test API get users returns an array", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        console.log(res.body);
        assert.isArray(res.body);
        done();
      });
  });
  it("Test API get users array first object username is Jojo", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        console.log(res.body);
        assert.equal(res.body[0].username, "Jojo");
        done();
      });
  });
  it("Test API get the last item in the  users array", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        assert.notEqual(res.body[res.body.length - 1], undefined);
        done();
      });
  });
  it("Test API post users returns 400 if no name", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .post("/users")
      .type("json")
      .send({
        age: 30,
        username: "jin92",
      })
      .end((err, res) => {
        console.log(res.status);
        assert.equal(res.status, 400);
        done();
      });
  });
  it("Test API post users returns 400 if no age", (done) => {
    //request() method is used from chai to create an http request given to the server
    chai
      .request("http://localhost:4000")
      .post("/users")
      .type("json")
      .send({
        age: 30,
        username: "jin92",
      })
      .end((err, res) => {
        console.log(res.status);
        assert.equal(res.status, 400);
        done();
      });
  });
});
*/
//S3 - ACTIVITY
it("Test API if artist can retrieved a response", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .get("/artists")
    .end((err, res) => {
      console.log(res.body);
      assert.isArray(res.body);
      done();
    });
});

it("Test API get the first object in the  artists array", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .get("/artists")
    .end((err, res) => {
      console.log();
      assert.notEqual(res.body[0].songs, undefined);
      done();
    });
});
it("Test API post artists returns 400 if no name", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .post("/artists")
    .type("json")
    .send({
      songs: ["Halata", "Singkamas"],
      username: "jin92",
    })
    .end((err, res) => {
      console.log(res.status);
      assert.equal(res.status, 400);
      done();
    });
});
it("Test API post artists returns 400 if no songs", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .post("/artists")
    .type("json")
    .send({
      age: 30,
      username: "jin92",
    })
    .end((err, res) => {
      console.log(res.status);
      assert.equal(res.status, 400);
      done();
    });
});

it("Test API post artists returns 400 if no album", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .post("/artists")
    .type("json")
    .send({
      album: "1993 album",
    })
    .end((err, res) => {
      console.log(res.status);
      assert.equal(res.status, 400);
      done();
    });
});

it("Test API post artists returns 400 if isActive property is false", (done) => {
  //request() method is used from chai to create an http request given to the server
  chai
    .request("http://localhost:4000")
    .post("/artists")
    .type("json")
    .send({
      isActive: false,
    })
    .end((err, res) => {
      console.log(res.status);
      assert.equal(res.status, 400);
      done();
    });
});

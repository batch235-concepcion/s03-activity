//Basic imports
const express = require("express");

//Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let users = [
  {
    name: "Jojo Joestar",
    age: 25,
    username: "Jojo",
  },
  {
    name: "Dio Brando",
    age: 23,
    username: "Dio",
  },
  {
    name: "Jotaro Kujo",
    age: 28,
    username: "Jotaro",
  },
];

//ACTIVITY - S3

let artists = [
  {
    name: "Parokya Ni Edgar",
    songs: ["Gitara", "Halaga"],
    album: "2015 album",
    isActive: true,
  },
  {
    name: "Kamikazee",
    songs: ["Narda", "Martyr Nyebera"],
    album: "2016 album",
    isActive: true,
  },
  {
    name: "Siakol",
    songs: ["Buloy", "Tropa"],
    album: "2019 album",
    isActive: true,
  },
];

//SECTION FOR ROUTES AND CONTROLLER
app.get("/users", (req, res) => {
  return res.send(users);
});

app.post("/users", (req, res) => {
  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter NAME",
    });
  }
});

app.post("/users", (req, res) => {
  if (!req.body.hasOwnProperty("age")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter AGE",
    });
  }
});

//ACTIVITY - S3

app.get("/artists", (req, res) => {
  return res.send(artists);
});

app.post("/artists", (req, res) => {
  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter NAME",
    });
  }
});

app.post("/artists", (req, res) => {
  if (!req.body.hasOwnProperty("songs")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter SONGS",
    });
  }
});

app.post("/artists", (req, res) => {
  if (!req.body.hasOwnProperty("album")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter ALBUM",
    });
  }
});

app.post("/artists", (req, res) => {
  if (!req.body.hasOwnProperty("isActive")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter isActive",
    });
  }
});

app.listen(port, () => console.log(`Server Running at port ${port}`));
